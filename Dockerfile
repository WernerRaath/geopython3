FROM python:3.6-slim-buster

# ARGS
ARG TIMEZONE="Europe/London"
ARG LOCALE="en_US.UTF-8"

# ENV settings
ENV TZ=${TIMEZONE} \
        DEBIAN_FRONTEND="noninteractive" \
        DEB_BUILD_DEPS="tzdata build-essential apt-utils git" \
        DEB_PACKAGES="freetds-dev g++ gcc libpq-dev locales gdal-bin libgdal-dev python-numpy python3-gdal libsqlite3-mod-spatialite curl"

# Run all installs
RUN \
    # Install dependencies
    apt-get update \
    && apt-get --no-install-recommends install -y ${DEB_BUILD_DEPS} ${DEB_PACKAGES} \
    # Timezone
    && cp /usr/share/zoneinfo/${TZ} /etc/localtime \
    && dpkg-reconfigure tzdata \
    # Locale
    && sed -i -e "s/# ${LOCALE} UTF-8/${LOCALE} UTF-8/" /etc/locale.gen \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale LANG=${LOCALE} \
    && echo "For ${TZ} date=$(date)" && echo "Locale=$(locale)" \
    # Cleanup TODO: remove unused Locales and TZs
    && apt-get remove --purge ${DEB_BUILD_DEPS} -y \
    && apt autoremove -y  \
    && rm -rf /var/lib/apt/lists/* \
#    && ln -s /usr/bin/python3.6 /usr/bin/python \
#    && ln -s /usr/bin/pip3 /usr/bin/pip \
    && pip install --upgrade pip \
    && pip install numpy \
    && pip install GDAL==2.4.2 \
    && python -c "from osgeo import gdal, ogr, osr" \
    && ogr2ogr --formats | grep PostgreSQL \
    && ogr2ogr --formats | grep MSSQLSpatial
