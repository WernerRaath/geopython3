FROM registry.gitlab.com/wernerraath/geopython3:latest

RUN apt-get update \
    && apt-get install -y python-psycopg2 \
    && pip install jinja2 \
        psycopg2